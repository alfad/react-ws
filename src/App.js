// src/App.js
import React from 'react';
import WebSocketComponent from './components/WebsocketComponent';

function App() {
  return (
    <div className="App">
      <h1>React App with WebSocket</h1>
      <WebSocketComponent />
    </div>
  );
}

export default App;
