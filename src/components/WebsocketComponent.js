// src/WebSocketComponent.js
import React, { useEffect, useRef, useState } from 'react';

const WebSocketComponent = () => {
  const [text, setText] = useState('');
  const wsRef = useRef(null);

  useEffect(() => {
    // Replace 'ws://localhost:8080' with your WebSocket server endpoint
    // const socket = new WebSocket('ws://192.168.0.2:3000/order/notification/13?token=1234567890');
    const socket = new WebSocket('ws://192.168.0.2:3000/v1/order/real-time/13');

    socket.onopen = () => {
      console.log('WebSocket connection established!');
      wsRef.current = socket;
    };

    socket.onmessage = (event) => {
      // Handle incoming messages here
      console.log('Received message:', event.data);
      setText(event.data);
    };

    socket.onclose = (event) => {
      console.log('WebSocket connection closed:', event);
    };

    // Clean up the WebSocket connection on unmount
    return () => {
      socket.close();
    };
  }, []);

  const handleSendPing = () => {
    if (wsRef.current) {
      // Send a ping message
      wsRef.current.send('ping');
      console.log('Sent ping message');
    } else {
      console.log('WebSocket connection not established.');
    }
  };

  return (
    <>
      WebSocket connection established!

      <div>{text}</div>

      <button onClick={handleSendPing}>Send Ping</button>
    </>
  );
};

export default WebSocketComponent;
