// src/SockJSComponent.js
import React, { useEffect } from 'react';
import SockJS from 'sockjs-client';

const SockJSComponent = () => {
  useEffect(() => {
    // Replace 'http://localhost:8080/ws' with your SockJS server endpoint
    const socket = new SockJS('ws://192.168.0.2:3000');
    const stompClient = require('webstomp-client').over(socket);

    stompClient.connect({}, (frame) => {
      console.log('Connected to SockJS:', frame);

      stompClient.subscribe('/topic/some-topic', (message) => {
        // Handle incoming messages here
        console.log('Received message:', message.body);
      });
    });

    // Clean up the WebSocket connection on unmount
    return () => {
      stompClient.disconnect();
    };
  }, []);

  return <div>WebSocket connection established!</div>;
};

export default SockJSComponent;
